package batholemew.tankmanager.activitys;

import android.app.ListActivity;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import batholemew.tankmanager.Nodes;
import batholemew.tankmanager.Observer;
import batholemew.tankmanager.ObserverListView;
import batholemew.tankmanager.R;
import batholemew.tankmanager.connectivity.GetReceiptDetailsQuery;
import batholemew.tankmanager.connectivity.LoadAllReceiptsQuery;

/**
 * Activity zum Auflisten aller einzelner gespeicherter Tankquittungen.
 * 
 * @author Oliver Griebel
 * @version 16.12.2013
 */
public class ListReceipts extends ListActivity implements Observer,
		ObserverListView, Nodes {

	// Ausgewählter Eintrag
	private String numberplate;
	private String password;
	private View chosenView; // VARIABLE VERMEIDEN (eventuell durch weitergabe
								// in async)

	/**
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// Bildschirmansicht festlegen
		setContentView(R.layout.layout_list);

		// Variablendeklaration
		ListView listView;
		Button buttonBack;

		// Variablen initialisieren
		listView = getListView();
		buttonBack = (Button) findViewById(R.id.buttonBack);

		// Initialisierung der Activity
		initiateActivity();

		// ListView bearbeiten
		listView.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				// ListView laden
				loadReceipts(view);
			}
		});

		// Reaktion auf Klick
		buttonBack.setOnClickListener(new OnClickListener() {

			/**
			 * (non-Javadoc)
			 * 
			 * @see android.view.View.OnClickListener#onClick(android.view.View)
			 */
			public void onClick(View v) {

				// Beenden der aktuellen Activity
				finish();
			}
		});

//		System.out.println("num:" + numberplate + ", pw:" + password);
	}

	/**
	 * Initialisierung der Activity
	 */
	private void initiateActivity() {

		// Variablendeklaration
		Intent intent;

		// Übergabeparameter erhalten
		intent = getIntent();

		// Übergabeparameter in Textfelder schreiben
		numberplate = intent.getStringExtra(TAG_NUMBERPLATE);
		password = intent.getStringExtra(TAG_PASSWORD);

		// Initialisieren des Wertes Kilometerstand
		((TextView) findViewById(R.id.numberplate)).setText(numberplate);

		// Laden aller receipts in einem AsyncTask
		new LoadAllReceiptsQuery(ListReceipts.this).execute(numberplate);
	}

	/**
	 * Laden der einzelnen Rechnungen aus der Datenbank
	 * 
	 * @param view
	 *            View representing the list item
	 */
	private void loadReceipts(View view) {

		// Variablendeklaration
		// String numberplate;
		String mileage;

		// Ausgewählte view speichern
		chosenView = view;// VARIABLE VERMEIDEN (eventuell durch
							// weitergabe in async) siehe line 28

		// Variablen initialisieren
		// numberplate = ((TextView) findViewById(R.id.numberplate)).getText()
		// .toString();
		mileage = ((TextView) chosenView.findViewById(R.id.mileage)).getText()
				.toString();

		// Receipt details aus einem AsyncTask herausfinden
		new GetReceiptDetailsQuery(ListReceipts.this).execute(mileage,
				numberplate);
	}

	/**
	 * Reaktion der Activity List receipt
	 * 
	 * @param requestCode
	 *            The integer request code originally supplied to
	 *            startActivityForResult(), allowing you to identify who this
	 *            result came from.
	 * @param resultCode
	 *            The integer result code returned by the child activity through
	 *            its setResult().
	 * @param data
	 *            An Intent, which can return result data to the caller (various
	 *            data can be attached to Intent "extras").
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		// Abfrage, ob resultCode 100 ist
		// beim entfernen/hinzufügen eines Produkts, neu laden der activity
		if (resultCode == 100) {

			// Variablendeklaration
			Intent intent;

			// getter für Intent
			intent = getIntent();

			// beenden der Activity
			finish();

			// neustart der Activity
			startActivity(intent);
		}
	}

	/**
	 * Auswertung der URL-Abfrage
	 * 
	 * @param receiptDetails
	 *            detailierte Ergebnisse der Serverabfrage
	 */
	public void updateActivity(HashMap<String, String> receiptDetails) {

		// bei Erfolg updaten
		if (receiptDetails.get(TAG_SUCCESS).equals("true")) {

			// Variablendeklaration
			String date;
			String mileage;
			String literprice;
			String charge;
			// String numberplate;
			String type;
			Intent intent;

			// Parameter holen
			date = receiptDetails.get(TAG_DATE);
			mileage = ((TextView) chosenView.findViewById(R.id.mileage))
					.getText().toString();
			literprice = receiptDetails.get(TAG_LITERPRICE);
			charge = receiptDetails.get(TAG_CHARGE);
			// numberplate = ((TextView)
			// findViewById(R.id.numberplate)).getText()
			// .toString();
			type = "edit";

			// Neue Intent starten
			intent = new Intent(getApplicationContext(), EditReceipt.class);

			// Kilometerstand an andere Activity beim start übergeben
			intent.putExtra(TAG_DATE, date);
			intent.putExtra(TAG_MILEAGE, mileage);
			intent.putExtra(TAG_LITERPRICE, literprice);
			intent.putExtra(TAG_CHARGE, charge);
			intent.putExtra(TAG_TYPE, type);
			intent.putExtra(TAG_NUMBERPLATE, numberplate);
			intent.putExtra(TAG_PASSWORD, password);

			// starting new activity and expecting some response back
			startActivityForResult(intent, 100);
		}
	}

	/**
	 * Auswertung der URL-Abfrage
	 * 
	 * @param loginDetails
	 *            detailierte Ergebnisse der Serverabfrage
	 */
	public void updateListView(ArrayList<HashMap<String, String>> details) {

		// Variablendeklaration
		String success;

		// Variablen Initialisierung
		success = "false";

		if (details != null) {
			// Abfrage, ob erfolgreich
			success = details.get(0).get(TAG_SUCCESS);
		}

		// bei Erfolg updaten
		if (success.equals("true")) {

			// Adapterdeklaration
			ListAdapter adapter;

			// Adapter initialisieren
			adapter = new SimpleAdapter(getBaseContext(), details,
					R.layout.list_item, new String[] { TAG_MILEAGE,
							TAG_LITERPRICE, TAG_CHARGE }, new int[] {
							R.id.mileage, R.id.literprice, R.id.charge });

			// listview erneuern
			setListAdapter(adapter);
		} else {

			// Variablendeklaration
			Intent intent;
			// String numberplate;

			// Variable initialisieren
			// numberplate = ((TextView)
			// findViewById(R.id.numberplate)).getText()
			// .toString();

			// Keine Rechnungen vorhanden => Activity zum erzeugen einer neuen
			// aufrufen
			intent = new Intent(getApplicationContext(), EditReceipt.class);

			// Kilometerstand an andere Activity beim start übergeben
			intent.putExtra(TAG_NUMBERPLATE, numberplate);
			intent.putExtra(TAG_PASSWORD, password);

			// Schließt alle anderen Activitys
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			// Startet Activity und erwartet eine Antwort
			startActivityForResult(intent, 100);
		}
	}
}
