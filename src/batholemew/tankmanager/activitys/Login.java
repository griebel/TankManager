package batholemew.tankmanager.activitys;

import java.util.HashMap;

import batholemew.tankmanager.Nodes;
import batholemew.tankmanager.Observer;
import batholemew.tankmanager.R;
import batholemew.tankmanager.connectivity.LoginQuery;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Login-Bereich der Anwendung und gleichzeitig Startbildschirm.
 * 
 * @author Oliver Griebel
 * @version 16.12.2013
 */
public class Login extends Activity implements Observer, Nodes {

	private String numberplate;
	private String password;

	/**
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// Bildschirmansicht festlegen
		setContentView(R.layout.layout_login);

		// Dummyvariablen
//		((TextView) findViewById(R.id.editTextNumberplate)).setText("alf");
//		((TextView) findViewById(R.id.editTextPassword)).setText("hallo");

		// Variablendeklaration
		Button buttonLogin;

		// Initialisierung der Buttons
		buttonLogin = (Button) findViewById(R.id.buttonLogin);
		Button buttonQuit = (Button) findViewById(R.id.buttonQuit);

		// Reaktion auf Klick
		buttonLogin.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				login();
			}
		});

		// Reaktion auf Klick
		buttonQuit.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				// Aktuelle activity beenden
				finish();
			}
		});
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		// Abfrage, ob "Back"-Knopf gedr�ckt wurde
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {

			// es passiert nichts
		}

		return false;
	}

	/**
	 * Einloggen und wechseln zum Main-Screen
	 */
	private void login() {

		// Variablendeklaration

		// Nummernschild und Passwort auslesen
		numberplate = ((TextView) findViewById(R.id.editTextNumberplate))
				.getText().toString();
		password = ((TextView) findViewById(R.id.editTextPassword)).getText()
				.toString();

		// Abfrage auf korrekte Userdaten mittels eines AsyncTasks
		new LoginQuery(this).execute(numberplate, password);
	}

	/**
	 * Auswertung der URL-Abfrage
	 * 
	 * @param loginDetails
	 *            detailierte Ergebnisse der Serverabfrage
	 */
	public void updateActivity(HashMap<String, String> loginDetails) {

		// Variablendeklaration
		Toast toast;

		// Kontrolle, ob abfrage korrekt war
		if (loginDetails.get(TAG_SUCCESS).equals("true")) {

			// Variablendeklaration
//			String purchasemileage;
			Intent intent;

			// Abrufen der Parameter
//			purchasemileage = loginDetails.get(TAG_PURCHASEMILEAGE);
 
			// Neue Intent initialisieren
			intent = new Intent(getApplicationContext(), Menu.class);
 
			// Kilometerstand an andere Activity beim start �bergeben
			intent.putExtra(TAG_NUMBERPLATE, numberplate);
			intent.putExtra(TAG_PASSWORD, password);
//			intent.putExtra(TAG_PURCHASEMILEAGE, purchasemileage);
 
			// Zum Auswahlmen� wechseln
			startActivity(intent);

			// Login-Activity beenden
			finish();

			// Einloggen erfolgreich
			toast = Toast.makeText(this, getString(R.string.loginsuccessful),
					Toast.LENGTH_SHORT);
		} else {

			// Fehler beim Einloggen
			toast = Toast.makeText(this, getString(R.string.loginfailed),
					Toast.LENGTH_LONG);

		}

		// Meldung zu Login anzeigen
		toast.show();

	}
}
