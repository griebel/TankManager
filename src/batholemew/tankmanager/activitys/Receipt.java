package batholemew.tankmanager.activitys;

import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import batholemew.tankmanager.Nodes;
import batholemew.tankmanager.Observer;
import batholemew.tankmanager.R;
import batholemew.tankmanager.connectivity.SaveReceiptDetailsQuery;

public abstract class Receipt extends Activity implements Nodes, Observer {

	// Unterscheidungsmerkmal zwischen bearbeiten und löschen
	protected String activityType;

	protected String numberplate;
	protected String password;

	// Kilometerstand
	protected String mileage;

	/**
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		// Bildschirmansicht festlegen
		setContentView(R.layout.layout_edit);

		// Variablendeklaration
		Button buttonSave;
		Button buttonBack;

		// Initialisierung der Buttons
		buttonSave = (Button) findViewById(R.id.buttonSave);
		buttonBack = (Button) findViewById(R.id.buttonBack);

		// Initialisierung der Activity
		initiateActivity();

		// Abhängig der Klasse Button initialisieren
		initButton();

		// Reaktion auf Save
		buttonSave.setOnClickListener(new OnClickListener() {

			/**
			 * (non-Javadoc)
			 * 
			 * @see android.view.View.OnClickListener#onClick(android.view.View)
			 */
			public void onClick(View v) {

				saveReceipt();
			}
		});

		// Reaktion auf Back
		buttonBack.setOnClickListener(new OnClickListener() {

			/**
			 * (non-Javadoc)
			 * 
			 * @see android.view.View.OnClickListener#onClick(android.view.View)
			 */
			public void onClick(View v) {

				// Beenden der aktuellen Activity
				finish();
			}
		});

		System.out.println("num:" + numberplate + ", pw:" + password);
	}

	protected abstract void initButton();

	/**
	 * Initialisierung der Activity
	 */
	private void initiateActivity() {

		// Variablendeklaration
		Intent i;
		EditText editTextNumberplate;
//		String numberPlate;

		// Übergabeparameter erhalten
		i = getIntent();

		// Initialisierung des Zugriffs auf die Textfelder
		editTextNumberplate = (EditText) findViewById(R.id.editTextNumberplate);

		// Zuweisen der Strings
		numberplate = i.getStringExtra(TAG_NUMBERPLATE);
		password = i.getStringExtra(TAG_PASSWORD);
		activityType = i.getStringExtra(TAG_TYPE);

		// Übergabeparameter in Textfelder schreiben
		editTextNumberplate.setText(numberplate);

		// Textfelder laden
		loadDetails();
	}

	protected abstract void loadDetails();
	
	/**
	 * Speichern der Rechnung in die Datenbank
	 */
	private void saveReceipt() {

		// Variablendeklaration
		String oldmileage;
		EditText editTextDate;
		EditText editTextMileage;
		EditText editTextLiterPrice;
		EditText editTextCharge;
//		EditText editTextNumberplate;
		String date;
		String literPrice;
		String charge;
//		String numberPlate;

		// Initialisierung des Zugriffs auf die Textfelder
//		editTextNumberplate = (EditText) findViewById(R.id.editTextNumberplate);
		editTextDate = (EditText) findViewById(R.id.editTextDate);
		editTextMileage = (EditText) findViewById(R.id.editTextMileage);
		editTextLiterPrice = (EditText) findViewById(R.id.editTextLiterPrice);
		editTextCharge = (EditText) findViewById(R.id.editTextCharge);

		// Ursprünglicher Kilometerstand merken
		oldmileage = mileage;

		// Holen der aktuellen Strings
		date = editTextDate.getText().toString();
		mileage = editTextMileage.getText().toString();
		literPrice = editTextLiterPrice.getText().toString();
		charge = editTextCharge.getText().toString();
//		numberPlate = editTextNumberplate.getText().toString();

		// Speichern der aktuellen Parameter mittels eines AsyncTasks
		new SaveReceiptDetailsQuery(Receipt.this).execute(activityType, date,
				oldmileage, mileage, literPrice, charge, numberplate);
	}

	/**
	 * Auswertung der URL-Abfrage
	 * 
	 * @param loginDetails
	 *            detailierte Ergebnisse der Serverabfrage
	 */
	public void updateActivity(HashMap<String, String> receiptDetails) {

		// Variablendeklaration
		Toast toast;

		if (receiptDetails.get(TAG_SUCCESS).equals("true")) {

			// Variablendeklaration
			Intent i;

			// Intent initialisieren
			i = getIntent();

			// Senden des Resultcodes 100, da update erfolgt ist
			setResult(100, i);

			// Activity beenden
			finish();

			// Speichern erfolgreich
			toast = Toast.makeText(this, getString(R.string.successful),
					Toast.LENGTH_SHORT);
		} else {

			// Fehler beim speichern
			toast = Toast.makeText(this, getString(R.string.failed),
					Toast.LENGTH_LONG);
		}

		// Meldung ausgeben
		toast.show();
	}

}
