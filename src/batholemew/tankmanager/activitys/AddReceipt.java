package batholemew.tankmanager.activitys;

import batholemew.tankmanager.R;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class AddReceipt extends Receipt {

	protected void initButton() {
		
		// Variablendeklaration
		Button buttonClear;

		// Initialisierung der Buttons
		buttonClear = (Button) findViewById(R.id.buttonClear);

		// Reaktion auf Clear
		buttonClear.setOnClickListener(new OnClickListener() {

			/**
			 * (non-Javadoc)
			 * 
			 * @see android.view.View.OnClickListener#onClick(android.view.View)
			 */
			public void onClick(View v) {

				clearReceipt();
			}
		});

		// Button sichtbar machen
		buttonClear.setVisibility(View.VISIBLE);
	}

	/**
	 * Felder der Activity leeren
	 */
	private void clearReceipt() {

		// Variablendeklaration
		EditText editTextDate;
		EditText editTextMileage;
		EditText editTextLiterPrice;
		EditText editTextCharge;

		// Initialisierung des Zugriffs auf die Textfelder
		editTextDate = (EditText) findViewById(R.id.editTextDate);
		editTextMileage = (EditText) findViewById(R.id.editTextMileage);
		editTextLiterPrice = (EditText) findViewById(R.id.editTextLiterPrice);
		editTextCharge = (EditText) findViewById(R.id.editTextCharge);

		// Textfelder leeren
		editTextDate.setText("");
		editTextMileage.setText("");
		editTextLiterPrice.setText("");
		editTextCharge.setText("");
	}

	@Override
	protected void loadDetails() {
		
	}
}
