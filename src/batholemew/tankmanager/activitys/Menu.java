package batholemew.tankmanager.activitys;

import batholemew.tankmanager.Nodes;
import batholemew.tankmanager.R;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

/**
 * Auswahlmen� der Anwendung <br>
 * M�gliche Optionen:<br>
 * Rechnungen Anzeigen<br>
 * Rechnung hinzuf�gen<br>
 * Statistik (sp�ter)
 * 
 * @author Oliver Griebel
 * @version 16.12.2013
 */
public class Menu extends Activity implements Nodes {

	// Parameter des Fahrzeugs
	private String numberplate;
	private String password;
	private String purchasemileage; // Aktuell noch ungenutzt (f�r die Statistik
									// wichtig)

	/**
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// Bildschirmansicht festlegen
		setContentView(R.layout.layout_menu);

		// Variablendeklaration
		Button buttonListReceipt;
		Button buttonAddReceipt;
		Button buttonLogout;
		Button buttonQuit;

		// Initialisierung der Buttons
		buttonListReceipt = (Button) findViewById(R.id.buttonListReceipt);
		buttonAddReceipt = (Button) findViewById(R.id.buttonAddReceipt);
		buttonLogout = (Button) findViewById(R.id.buttonLogout);
		buttonQuit = (Button) findViewById(R.id.buttonQuit);

		// Initialisierung der Activity
		initiateActivity();

		// Reaktion auf Klick
		buttonListReceipt.setOnClickListener(new OnClickListener() {

			/**
			 * (non-Javadoc)
			 * 
			 * @see android.view.View.OnClickListener#onClick(android.view.View)
			 */
			public void onClick(View v) {

				startListActivity();
			}
		});

		// Reaktion auf Klick
		buttonAddReceipt.setOnClickListener(new OnClickListener() {

			/**
			 * (non-Javadoc)
			 * 
			 * @see android.view.View.OnClickListener#onClick(android.view.View)
			 */
			public void onClick(View v) {

				// Methode zum hinzuf�gen einer neuen Rechnung
				startAddReceipt();
			}
		});

		// Reaktion auf Klick
		buttonLogout.setOnClickListener(new OnClickListener() {

			/**
			 * (non-Javadoc)
			 * 
			 * @see android.view.View.OnClickListener#onClick(android.view.View)
			 */
			public void onClick(View v) {

				// Methode zum ausloggen
				logout();
			}
		});

		// Reaktion auf Klick
		buttonQuit.setOnClickListener(new OnClickListener() {

			/**
			 * (non-Javadoc)
			 * 
			 * @see android.view.View.OnClickListener#onClick(android.view.View)
			 */
			public void onClick(View v) {

				// Beenden der aktuellen Activity
				finish();
			}
		});
	}

	/**
	 * Initialisierung der Activity
	 */
	public void initiateActivity() {

		// Variablendeklaration
		Intent intent;

		// �bergabeparameter erhalten
		intent = getIntent();

		// �bergabeparameter in Textfelder schreiben
		numberplate = intent.getStringExtra(TAG_NUMBERPLATE);
		password = intent.getStringExtra(TAG_PASSWORD);
		purchasemileage = intent.getStringExtra(TAG_PURCHASEMILEAGE);

	}

	/**
	 * Zur Auflistung der Rechnungen wechseln
	 */
	public void startListActivity() {

		// Variablendeklaration
		Intent intent;

		// Initialisierung eines neuen Intent-Objekts
		intent = new Intent();

		// Definition einer Klasse f�r intent mit der Klasse
		// ActivityList
		intent.setClass(getBaseContext(), ListReceipts.class);

		// Kilometerstand an andere Activity beim start �bergeben
		intent.putExtra(TAG_NUMBERPLATE, numberplate);
		intent.putExtra(TAG_PASSWORD, password);

		// Starten der anderen Activity
		startActivity(intent);
	}

	/**
	 * Zum Einf�gen einer neuen Rechnung wechseln
	 */
	public void startAddReceipt() {

		// Variablendeklaration
		Intent intent;
		String type;

		// Initialisierung eines neuen Intent-Objekts
		intent = new Intent();
		type = "add";

		// Definition einer Klasse f�r intent mit der Klasse
		// ActivityAdd
		intent.setClass(getBaseContext(), AddReceipt.class);

		// Kilometerstand an andere Activity beim start �bergeben
		intent.putExtra(TAG_NUMBERPLATE, numberplate);
		intent.putExtra(TAG_PASSWORD, password);
		intent.putExtra(TAG_TYPE, type);

		// Aktivieren der anderen Activity
		startActivity(intent);
	}

	/**
	 * Ausloggen und wechseln zum Login-Screen
	 */
	public void logout() {

		// Variablendeklaration
		Intent intent;

		// Initialisierung eines neuen Intent-Objekts
		intent = new Intent();

		// Definition einer Klasse f�r intent mit der Klasse
		// ActivityLogout
		intent.setClass(getBaseContext(), Login.class);

		// Aktivieren der anderen Activity
		startActivity(intent);

		// Beenden der aktuellen Activity
		finish();
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
	 */
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		// Abfrage, ob "Back"-Knopf gedr�ckt wurde
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {

			// Meldung, dass Button gesperrt ist
			Toast toast = Toast.makeText(this, getString(R.string.disabled),
					Toast.LENGTH_LONG);
			toast.show();
		}

		return false;
	}
}
