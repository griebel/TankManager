package batholemew.tankmanager.activitys;

import batholemew.tankmanager.R;
import batholemew.tankmanager.connectivity.DeleteReceiptQuery;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class EditReceipt extends Receipt {

	protected void initButton() {

		// Variablendeklaration
		Button buttonDelete;

		// Initialisierung der Buttons
		buttonDelete = (Button) findViewById(R.id.buttonDelete);

		// Reaktion auf Delete
		buttonDelete.setOnClickListener(new OnClickListener() {

			/**
			 * (non-Javadoc)
			 * 
			 * @see android.view.View.OnClickListener#onClick(android.view.View)
			 */
			public void onClick(View v) {

				deleteReceipt();
			}
		});

		// Button sichtbar machen
		buttonDelete.setVisibility(View.VISIBLE);
	}

	/**
	 * Entfernen einer Rechnung aus der Datenbank
	 */
	private void deleteReceipt() {

		// Variablendeklaration
//		EditText editTextNumberplate;

		// Initialisierung des Zugriffs auf das Textfeld
//		editTextNumberplate = (EditText) findViewById(R.id.editTextNumberplate);

		// Holen der aktuellen Strings
//		numberplate = editTextNumberplate.getText().toString();

		// Entfernen eines Parameters mittels eines AsyncTasks
		new DeleteReceiptQuery(EditReceipt.this).execute(mileage, numberplate);
	}

	@Override
	protected void loadDetails() {
		
		// Variablendeklaration
		Intent i;
		EditText editTextDate;
		EditText editTextMileage;
		EditText editTextLiterPrice;
		EditText editTextCharge;
		String date;
		String literPrice;
		String charge;
		
		// ‹bergabeparameter erhalten
		i = getIntent();
		
		// Initialisierung des Zugriffs auf die Textfelder
		editTextDate = (EditText) findViewById(R.id.editTextDate);
		editTextMileage = (EditText) findViewById(R.id.editTextMileage);
		editTextLiterPrice = (EditText) findViewById(R.id.editTextLiterPrice);
		editTextCharge = (EditText) findViewById(R.id.editTextCharge);

		// Zuweisen der Strings
		date = i.getStringExtra(TAG_DATE);
		mileage = i.getStringExtra(TAG_MILEAGE);
		literPrice = i.getStringExtra(TAG_LITERPRICE);
		charge = i.getStringExtra(TAG_CHARGE);

		// ‹bergabeparameter in Textfelder schreiben
		editTextDate.setText(date);
		editTextMileage.setText(mileage);
		editTextLiterPrice.setText(literPrice);
		editTextCharge.setText(charge);
	}
}
