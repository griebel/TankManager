package batholemew.tankmanager;

/**
 * Interface f�r alle Knotenbezeichnungen
 * 
 * @author Batholemew
 * @version 16.12.2013
 */
public interface Nodes {

	// alle verwendeten JSON Knotennamen
	static final String TAG_CHARGE = "charge";
	static final String TAG_DATE = "date";
	static final String TAG_LITERPRICE = "literprice";
	static final String TAG_MESSAGE = "message";
	static final String TAG_MILEAGE = "mileage";
	static final String TAG_NUMBERPLATE = "numberplate";
	static final String TAG_OLDMILEAGE = "oldmileage";
	static final String TAG_PASSWORD = "password";
	static final String TAG_PURCHASEMILEAGE = "purchasemileage";
	static final String TAG_RECEIPT = "receipt";
	static final String TAG_RECEIPTS = "receipts";
	static final String TAG_SUCCESS = "success";
	static final String TAG_TYPE = "activityType";

}
