package batholemew.tankmanager.connectivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import batholemew.tankmanager.Nodes;
import batholemew.tankmanager.R;
import batholemew.tankmanager.URLs;
import batholemew.tankmanager.activitys.Login;
import android.app.ProgressDialog;
import android.os.AsyncTask;

/**
 * Asynchroner Task zum Pr�fen der LoginDaten.
 * 
 * @author Oliver Griebel
 * @version 16.12.2013
 */
public class LoginQuery extends
		AsyncTask<String, Void, HashMap<String, String>> implements Nodes, URLs {

	// aktuell zu bearbeitende Aktivity
	private Login activity;

	// Progress Dialog
	private ProgressDialog pDialog;

	/**
	 * Initialisierung der Klasse
	 */
	public LoginQuery(Login activity) {
		this.activity = activity;
		pDialog = new ProgressDialog(activity);
	}

	/**
	 * Vor beginn des Backgroundtasks wird der Fortschrittsbalken gestartet
	 * */
	protected void onPreExecute() {

		super.onPreExecute();

		// Entfernen-Text
		String deleting = activity.getString(R.string.verifying);

		// Fortschrittsbalken bearbeiten
		pDialog.setMessage(deleting);
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
		pDialog.show();
	}

	/**
	 * Rechnungen im Hintergrund erhalten
	 * 
	 * @param args
	 *            Login und Passwort
	 * @return HashMap mit den Ergebnissen der HTTP-Abfrage
	 * */
	protected HashMap<String, String> doInBackground(String... args) {

		// Variablendeklaration
		String success;
		JSONParser jParser;
		String password;
		String message;
		String purchasemileage;
		String numberplate;
		HashMap<String, String> loginDetails;
		List<NameValuePair> params;
		// Variablen initialisieren
		loginDetails = new HashMap<String, String>();

		// Kontrolle, ob parameter vorhanden
		if (args.length == 2) {

			// �bergabeparameter zuordnen
			numberplate = args[0];
			password = args[1];

			// Suchkriterien
			params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(TAG_NUMBERPLATE, numberplate));
			params.add(new BasicNameValuePair(TAG_PASSWORD, password));

			// JSON Parser initialisieren
			jParser = new JSONParser();

			// Webabfrage des logins mittels JSON Parser
			JSONObject json = jParser
					.makeHttpRequest(url_login, "GET", params);

			try {

				// Erfolgsabfrage
				success = json.getString(TAG_SUCCESS);

				// Hinzuf�gen der Ergebnisse der Knoten
				loginDetails.put(TAG_SUCCESS, success);

				if (success.equals("true")) {

					// Umwandeln des JSONObjektes in einen String
					purchasemileage = json.getString(TAG_PURCHASEMILEAGE);

					// Hinzuf�gen der Ergebnisse der Knoten
					loginDetails.put(TAG_PURCHASEMILEAGE, purchasemileage);
					loginDetails.put(TAG_NUMBERPLATE, numberplate);
				} else if (success.equals("false")) {

					// Fehlerursache abfragen
					message = json.getString(TAG_MESSAGE);

					// Hinzuf�gen der Ergebnisse der Knoten
					loginDetails.put(TAG_MESSAGE, message);
				}
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}
		return loginDetails;
	}

	/**
	 * ProgressDialog ausblenden und Activity aktualisieren
	 * 
	 * @param activity
	 *            �bergabeparameter zur Activity
	 * */
	protected void onPostExecute(HashMap<String, String> loginDetails) {

		// Progress-Dialog ausblenden
		pDialog.dismiss();

		// Auswertung senden
		activity.updateActivity(loginDetails);
	}
}
