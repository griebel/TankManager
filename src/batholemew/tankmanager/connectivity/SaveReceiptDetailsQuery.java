package batholemew.tankmanager.connectivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import batholemew.tankmanager.Nodes;
import batholemew.tankmanager.R;
import batholemew.tankmanager.URLs;
import batholemew.tankmanager.activitys.Receipt;

import android.app.ProgressDialog;
import android.os.AsyncTask;

/**
 * Asynchroner Task zum speichern einer Rechnung.
 * 
 * @author Oliver Griebel
 * @version 16.12.2013
 */
public class SaveReceiptDetailsQuery extends
		AsyncTask<String, Void, HashMap<String, String>> implements Nodes, URLs {

	// aktuell zu bearbeitende Aktivity
	private Receipt activity;

	// Progress Dialog
	private ProgressDialog pDialog;

	/**
	 * Initialisierung der Klasse
	 */
	public SaveReceiptDetailsQuery(Receipt activity) {
		this.activity = activity;
		pDialog = new ProgressDialog(activity);
	}

	/**
	 * Vor beginn des Backgroundtasks wird der Fortschrittsbalken gestartet
	 * */
	protected void onPreExecute() {

		super.onPreExecute();

		// Entfernen-Text
		String saving = activity.getString(R.string.saving);

		// Fortschrittsbalken bearbeiten
		pDialog.setMessage(saving);
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
		pDialog.show();
	}

	/**
	 * Rechnung im Hintergrund speichern
	 * 
	 * @param args
	 *            Activitytyp, Datum, alter Kilometerstand, Kilometerstand,
	 *            Literpreis, Menge und Nummernschild
	 * @return HashMap mit den Ergebnissen der HTTP-Abfrage
	 * */
	protected HashMap<String, String> doInBackground(String... args) {

		// Variablendeklaration
		String activityType;
		String date;
		String oldmileage;
		String mileage;
		String literPrice;
		String charge;
		String numberPlate;
		String success;
		String message;
		JSONParser jParser;
		List<NameValuePair> params;
		HashMap<String, String> receiptDetails;
		JSONObject json;

		// Variablen initialisieren
		receiptDetails = new HashMap<String, String>();

		// Kontrolle, ob parameter vorhanden
		if (args.length == 7) {

			// Übergabeparameter zuordnen
			activityType = args[0];
			date = args[1];
			oldmileage = args[2];
			mileage = args[3];
			literPrice = args[4];
			charge = args[5];
			numberPlate = args[6];

			// Name-Wert-Paar initialisieren und anlegen
			params = new ArrayList<NameValuePair>();

			// Name-Wert-Paar initialisieren und anlegen
			params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(TAG_DATE, date));
			params.add(new BasicNameValuePair(TAG_OLDMILEAGE, oldmileage));
			params.add(new BasicNameValuePair(TAG_MILEAGE, mileage));
			params.add(new BasicNameValuePair(TAG_LITERPRICE, literPrice));
			params.add(new BasicNameValuePair(TAG_CHARGE, charge));
			params.add(new BasicNameValuePair(TAG_NUMBERPLATE, numberPlate));

			// JSON Parser initialisieren
			jParser = new JSONParser();

			// Webabfrage des receipts mittels JSON Parser
			if (activityType.equals("edit")) {

				// URL zum Ändern eines Eintrags
				json = jParser.makeHttpRequest(url_update_receipt, "GET",
						params);
			} else {

				// URL zum hinzufügen eines Eintrags
				json = jParser.makeHttpRequest(url_create_receipt, "GET",
						params);
			}

			try {

				// Erfolgsabfrage
				success = json.getString(TAG_SUCCESS);

				// Hinzufügen der Ergebnisse der Knoten
				receiptDetails.put(TAG_SUCCESS, success);

				if (success.equals("false")) {

					// Fehlerursache abfragen
					message = json.getString(TAG_MESSAGE);

					// Hinzufügen der Ergebnisse der Knoten
					receiptDetails.put(TAG_MESSAGE, message);
				}
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}
		return receiptDetails;
	}

	/**
	 * ProgressDialog ausblenden und Activity aktualisieren
	 * 
	 * @param receiptDetails
	 *            Übergabeparameter zur Activity
	 * */
	protected void onPostExecute(HashMap<String, String> receiptDetails) {

		// Progress-Dialog ausblenden
		pDialog.dismiss();

		// Auswertung senden
		activity.updateActivity(receiptDetails);
	}
}
