package batholemew.tankmanager.connectivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 * Parser zum Auslesen der Datenbank mittels Webzugriff und JSON-Objekten
 * 
 * @author Oliver Griebel
 * @version 06.12.2013
 */
public class JSONParser {

	static InputStream is = null;
	static JSONObject jObj = null;
	static String json = "";

	/**
	 * Initialisierung der Klasse
	 */
	public JSONParser() {

	}

	/**
	 * Funktion zum einlesen von Webseiten und als JSONObjekte zur�ckgeben. Dies
	 * erfolgt �ber die HTTP-Methoden POST und GET
	 * 
	 * @param url
	 *            Website f�r den Zugriff
	 * @param method
	 *            zugriff mittels GET oder POST
	 * @param params
	 *            �bergabeparameter
	 * 
	 * @return JSONObjekt mit allen details
	 */
	public JSONObject makeHttpRequest(String url, String method,
			List<NameValuePair> params) {

		// Abfragen der Daten
		try {

			// Abfrage nach der anfragemethode
			if (method.equals("POST")) {
				// Abfragemethode ist POST

				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(url);
				// parameter anh�ngen
				httpPost.setEntity(new UrlEncodedFormEntity(params));

				// httpanfrage absenden
				HttpResponse httpResponse = httpClient.execute(httpPost);
				// Datensatz auslesen
				HttpEntity httpEntity = httpResponse.getEntity();
				// Datensatz als inpustream formatieren
				is = httpEntity.getContent();

			} else if (method.equals("GET")) {
				// Abfragemethode ist GET

				// neuer defaultHttpClient
				DefaultHttpClient httpClient = new DefaultHttpClient();
				// �bergabeparameter formatieren
				String paramString = URLEncodedUtils.format(params, "utf-8");
				// parameter anh�ngen
				url += "?" + paramString;
				// url festlegen
				HttpGet httpGet = new HttpGet(url);

				// httpanfrage absenden, antwort erhalten
				HttpResponse httpResponse = httpClient.execute(httpGet);
				// Datensatz auslesen
				HttpEntity httpEntity = httpResponse.getEntity();
				// Datensatz als inpustream formatieren
				is = httpEntity.getContent();
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Umwandeln der empfangenen Daten in einen String
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}

		// parsen des Strings in ein JSON objekt
		try {
			jObj = new JSONObject(json);
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
		}

		return jObj;

	}
}