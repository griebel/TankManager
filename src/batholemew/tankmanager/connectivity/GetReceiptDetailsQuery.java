package batholemew.tankmanager.connectivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import batholemew.tankmanager.Nodes;
import batholemew.tankmanager.R;
import batholemew.tankmanager.URLs;
import batholemew.tankmanager.activitys.ListReceipts;

import android.app.ProgressDialog;
import android.os.AsyncTask;

/**
 * Asynchroner Task zum detailierten auslesen einer Rechnung.
 * 
 * @author Oliver Griebel
 * @version 16.12.2013
 */
public class GetReceiptDetailsQuery extends
		AsyncTask<String, Void, HashMap<String, String>> implements Nodes, URLs {

	// aktuell zu bearbeitende Aktivity
	private ListReceipts listReceipts;

	// Progress Dialog
	private ProgressDialog pDialog;

	/**
	 * Initialisierung der Klasse
	 */
	public GetReceiptDetailsQuery(ListReceipts activity) {
		listReceipts = activity;
		pDialog = new ProgressDialog(activity);
	}

	/**
	 * Vor Beginn des Backgroundtasks wird der Fortschrittsbalken gestartet
	 * */
	protected void onPreExecute() {

		super.onPreExecute();

		// Laden-Text
		String loading = listReceipts.getString(R.string.loading);

		// Fortschrittsbalken bearbeiten
		pDialog.setMessage(loading);
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
		pDialog.show();
	}

	/**
	 * Rechnung im Hintergrund detailiert erhalten
	 * 
	 * @param args
	 *            Kilometerstand und Nummernschild
	 * @return HashMap mit den Ergebnissen der HTTP-Abfrage
	 * */
	protected HashMap<String, String> doInBackground(String... args) {

		// Variablendeklaration
		String success;
		String date;
		String literprice;
		String charge;
		JSONParser jParser;
		String mileage;
		String message;
		String numberplate;
		HashMap<String, String> receiptDetails;
		List<NameValuePair> params;

		// Variablen initialisieren
		receiptDetails = new HashMap<String, String>();

		// Kontrolle, ob parameter vorhanden
		if (args.length == 2) {

			// Übergabeparameter zuordnen
			mileage = args[0];
			numberplate = args[1];

			// Suchkriterien
			params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(TAG_MILEAGE, mileage));
			params.add(new BasicNameValuePair(TAG_NUMBERPLATE, numberplate));

			// JSON Parser initialisieren
			jParser = new JSONParser();

			// Webabfrage des receipts mittels JSON Parser
			JSONObject json = jParser.makeHttpRequest(url_get_receipt_details,
					"GET", params);

			try {

				// json success tag
				success = json.getString(TAG_SUCCESS);

				// Hinzufügen der Ergebnisse der Knoten
				receiptDetails.put(TAG_SUCCESS, success);

				if (success.equals("true")) {

					// Array mit der Rechnung erhalten
//					JSONArray receiptObj = json.getJSONArray(TAG_RECEIPT);

					// Erstes Element auslesen
					JSONObject receipt = json.getJSONObject(TAG_RECEIPT);

					// Umwandeln aller JSONObjekte zu Strings
					date = receipt.getString(TAG_DATE);
					literprice = receipt.getString(TAG_LITERPRICE);
					charge = receipt.getString(TAG_CHARGE);

					// Hinzufügen der Ergebnisse der Knoten
					receiptDetails.put(TAG_DATE, date);
					receiptDetails.put(TAG_LITERPRICE, literprice);
					receiptDetails.put(TAG_CHARGE, charge);
				} else if (success.equals("false")) {

					// Fehlerursache abfragen
					message = json.getString(TAG_MESSAGE);

					// Hinzufügen der Ergebnisse der Knoten
					receiptDetails.put(TAG_MESSAGE, message);
				}
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return receiptDetails;
	}

	/**
	 * ProgressDialog ausblenden und Activity aktualisieren
	 * 
	 * @param receiptDetails
	 *            Übergabeparameter zur Activity
	 * */
	protected void onPostExecute(HashMap<String, String> receiptDetails) {

		// Progress-Dialog ausblenden
		pDialog.dismiss();

		// Auswertung senden
		listReceipts.updateActivity(receiptDetails);
	}
}
