package batholemew.tankmanager.connectivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import batholemew.tankmanager.Nodes;
import batholemew.tankmanager.R;
import batholemew.tankmanager.URLs;
import batholemew.tankmanager.activitys.Receipt;

import android.app.ProgressDialog;
import android.os.AsyncTask;
/**
 * Asynchroner Task zum Entfernen einer Rechnung.
 * 
 * @author Oliver Griebel
 * @version 16.12.2013
 */
public class DeleteReceiptQuery extends
		AsyncTask<String, Void, HashMap<String, String>> implements Nodes, URLs {

	// aktuell zu bearbeitende Aktivity
	private Receipt activity;

	// Fortschrittsbalken
	private ProgressDialog pDialog;

	/**
	 * Initialisierung der Klasse
	 */
	public DeleteReceiptQuery(Receipt activity) {

		// Übergabeparameter
		this.activity = activity;
		pDialog = new ProgressDialog(activity);
	}

	/**
	 * Vor beginn des Backgroundtasks wird der Fortschrittsbalken gestartet
	 * */
	protected void onPreExecute() {

		super.onPreExecute();

		// Entfernen-Text
		String deleting = activity.getString(R.string.deleting);

		// Fortschrittsbalken bearbeiten
		pDialog.setMessage(deleting);
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
		pDialog.show();
	}

	/**
	 * Rechnung im Hintergrund entfernen
	 * 
	 * @param args
	 *            Kilometerstand und Nummernschild
	 * @return HashMap mit den Ergebnissen der HTTP-Abfrage
	 * */
	protected HashMap<String, String> doInBackground(String... args) {

		// Variablendeklaration
		HashMap<String, String> receiptDetails;
		String mileage;
		String numberplate;
		String success;
		String message;
		List<NameValuePair> params;
		JSONObject json;
		JSONParser jParser;

		// Variablen initialisieren
		receiptDetails = new HashMap<String, String>();

		// Kontrolle, ob parameter vorhanden
		if (args.length == 2) {

			// Übergabeparameter zuordnen
			mileage = args[0];
			numberplate = args[1];

			// Name-Wert-Paar initialisieren und anlegen
			params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(TAG_MILEAGE, mileage));
			params.add(new BasicNameValuePair(TAG_NUMBERPLATE, numberplate));

			// JSON Parser initialisieren
			jParser = new JSONParser();

			// Webabfrage des receipts mittels JSON Parser
			json = jParser.makeHttpRequest(url_delete_receipt, "GET", params);

			try {

				// Erfolgsabfrage
				success = json.getString(TAG_SUCCESS);

				// Hinzufügen der Ergebnisse der Knoten
				receiptDetails.put(TAG_SUCCESS, success);

				if (success.equals("false")) {

					// Fehlerursache abfragen
					message = json.getString(TAG_MESSAGE);

					// Hinzufügen der Ergebnisse der Knoten
					receiptDetails.put(TAG_MESSAGE, message);
				}
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}

		return receiptDetails;
	}

	/**
	 * ProgressDialog ausblenden und Activity aktualisieren
	 * 
	 * @param receiptDetails
	 *            Übergabeparameter zur Activity
	 * */
	protected void onPostExecute(HashMap<String, String> receiptDetails) {

		// Progress-Dialog ausblenden
		pDialog.dismiss();

		// Auswertung senden
		activity.updateActivity(receiptDetails);
	}
}
