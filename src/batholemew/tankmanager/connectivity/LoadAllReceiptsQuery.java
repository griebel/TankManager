package batholemew.tankmanager.connectivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import batholemew.tankmanager.Nodes;
import batholemew.tankmanager.R;
import batholemew.tankmanager.URLs;
import batholemew.tankmanager.activitys.ListReceipts;

import android.app.ProgressDialog;
import android.os.AsyncTask;

/**
 * Asynchroner Task zum auslesen aller Rechnungen eines bestimmten Fahrzeugs.
 * 
 * @author Oliver Griebel
 * @version 16.12.2013
 */
public class LoadAllReceiptsQuery extends
		AsyncTask<String, Void, ArrayList<HashMap<String, String>>> implements
		Nodes, URLs {

	// aktuell zu bearbeitende Aktivity
	private ListReceipts activity;

	// Progress Dialog
	private ProgressDialog pDialog;

	public LoadAllReceiptsQuery(ListReceipts activity) {
		this.activity = activity;
		pDialog = new ProgressDialog(activity);
	}

	/**
	 * Vor beginn des Backgroundtasks wird der Fortschrittsbalken gestartet
	 * */
	protected void onPreExecute() {

		super.onPreExecute();

		// Laden-Text
		String loading = activity.getString(R.string.loading);

		// Fortschrittsbalken bearbeiten
		pDialog.setMessage(loading);
		pDialog.setIndeterminate(false);
		pDialog.setCancelable(false);
		pDialog.show();
	}

	/**
	 * Rechnungen im Hintergrund erhalten
	 * 
	 * @param args
	 *            Nummernschild
	 * @return HashMap mit den Ergebnissen der HTTP-Abfrage
	 * */
	protected ArrayList<HashMap<String, String>> doInBackground(String... args) {

		// Variablendeklaration
		List<NameValuePair> params;
		String numberplate;
		ArrayList<HashMap<String, String>> receiptDetails;
		JSONParser jParser;
		JSONObject json;
		String success;
		HashMap<String, String> receipt;
		String mileage;
		String literprice;
		String charge;
		String message;
		JSONArray receipts;

		// Variablen initialisieren
		receiptDetails = new ArrayList<HashMap<String, String>>();

		// Kontrolle, ob parameter vorhanden
		if (args.length == 1) {

			// Übergabeparameter zuordnen
			numberplate = args[0];

			// Name-Wert-Paar initialisieren und anlegen
			params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(TAG_NUMBERPLATE, numberplate));

			// JSON Parser initialisieren
			jParser = new JSONParser();

			// Webabfrage des receipts mittels JSON Parser
			json = jParser.makeHttpRequest(url_get_all_receipts, "GET", params);

			try {

				// Erfolgsabfrage
				success = json.getString(TAG_SUCCESS);

				if (success.equals("false")) {

					// Erfolgsabfrage
					message = json.getString(TAG_MESSAGE);

					// Variable initialisieren
					receipt = new HashMap<String, String>();

					// Hinzufügen der Ergebnisse der Knoten
					receipt.put(TAG_SUCCESS, success);
					receipt.put(TAG_MESSAGE, message);

					// hinzufügen der HashMap zur ArrayList
					receiptDetails.add(receipt);
				} else if (success.equals("true")) {

					// Array mit den Rechnungen erhalten
					receipts = json.getJSONArray(TAG_RECEIPTS);

					// Schleife über alle Rechnungen
					for (int i = 0; i < receipts.length(); i++) {

						// Nächstes Element auslesen
						json = receipts.getJSONObject(i);

						// Umwandeln aller JSONObjekte zu Strings
						mileage = json.getString(TAG_MILEAGE);
						literprice = json.getString(TAG_LITERPRICE);
						charge = json.getString(TAG_CHARGE);

						// Variable initialisieren
						receipt = new HashMap<String, String>();

						// Hinzufügen der Ergebnisse der Knoten
						receipt.put(TAG_SUCCESS, success);
						receipt.put(TAG_MILEAGE, mileage);
						receipt.put(TAG_LITERPRICE, literprice);
						receipt.put(TAG_CHARGE, charge);

						// hinzufügen der HashMap zur ArrayList
						receiptDetails.add(receipt);
					}
				}
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}
		return receiptDetails;
	}

	/**
	 * ProgressDialog ausblenden und Activity aktualisieren
	 * 
	 * @param receiptDetails
	 *            Übergabeparameter zur Activity
	 * */
	protected void onPostExecute(
			ArrayList<HashMap<String, String>> receiptDetails) {

		// Progress-Dialog ausblenden
		pDialog.dismiss();

		// Auswertung senden
		activity.updateListView(receiptDetails);

	}
}
