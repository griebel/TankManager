package batholemew.tankmanager;

/**
 * Interface f�r alle URLs
 * 
 * @author Batholemew
 * @version 16.12.2013
 */
public interface URLs {

	// URL zu den PHP-Skripten
//	static final String url_basic = "http://batholemew.servebeer.com/tankmanager/";
	static final String url_basic = "http://www.olis-corner.de/tankmanager/";

	// einzelne PHP-Skripte
	static final String url_create_receipt = url_basic + "create_receipt.php";
	static final String url_delete_receipt = url_basic + "delete_receipt.php";
	static final String url_get_all_receipts = url_basic
			+ "get_all_receipts.php";
	static final String url_get_receipt_details = url_basic
			+ "get_receipt_details.php";
	static final String url_login = url_basic + "login.php";
	static final String url_update_receipt = url_basic + "update_receipt.php";

}
