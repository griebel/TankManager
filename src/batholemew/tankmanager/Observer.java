package batholemew.tankmanager;

import java.util.HashMap;

/**
 * Observer f�r die Activitys und Asynctasks
 * 
 * @author Batholemew
 * @version 16.12.2013
 */
public interface Observer {

	/**
	 * Auswertung der URL-Abfrage
	 * 
	 * @param details
	 *            detailierte Ergebnisse der Serverabfrage
	 */
	public void updateActivity(HashMap<String, String> details);
}