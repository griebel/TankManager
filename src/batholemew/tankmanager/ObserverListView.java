package batholemew.tankmanager;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Observer f�r die Activitys und Asynctasks
 * 
 * @author Batholemew
 * @version 16.12.2013
 */
public interface ObserverListView {

	/**
	 * Auswertung der URL-Abfrage
	 * 
	 * @param details
	 *            detailierte Ergebnisse der Serverabfrage
	 */
	public void updateListView(ArrayList<HashMap<String, String>> details);
}
